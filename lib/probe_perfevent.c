/*
 * This file is part of energy-meter.
 *
 * Author: Erven ROHOU
 * Copyright (c) 2018, 2021 Inria
 *
 * License: GNU Affero General Public License version 3.
 *
 */

#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/syscall.h>
#include <unistd.h>

#include "emon.h"
#include "probe_perfevent.h"

#include <linux/perf_event.h>

#define PREFIX_PATH "/sys/bus/event_source/devices/power/"


//
// Probe based on perf_event
//


static long perf_event_open(struct perf_event_attr *hw_event, pid_t pid,
                            int cpu, int group_fd, unsigned long flags)
{
  int ret = syscall(__NR_perf_event_open, hw_event, pid, cpu, group_fd, flags);
  return ret;
}


static double Probe_RAPL_raw_reading(EMProbe* p)
{
  int       n;
  long long val;

  assert(p->u.fd != -1);

  n = read(p->u.fd, &val, sizeof(val));
  if (n != sizeof(val)) {
    fprintf(stderr, "Something went wrong in raw_reading\n");
  }
  return (double)val;
}


#define NUM_RAPL_DOMAINS 4

static char* domain_names[NUM_RAPL_DOMAINS] = {
  (char*) "energy-cores",
  (char*) "energy-gpu",
  (char*) "energy-pkg",
  (char*) "energy-ram",
};

static char* display_names[NUM_RAPL_DOMAINS] = {
  (char*) "Cores",
  (char*) "GPU",
  (char*) "Package",
  (char*) "RAM",
};


// return -1 in case of failure
int init_RAPL(EMonitor* em)
{
  FILE*      f;
  int        type = -1;
  int        num_cores;
  char       filename[BUFSIZ];
  char       units[100];   /* FIXME */
  struct perf_event_attr attr = { 0 };
  double scale;

  // int core = 0;   /* FIXME: handle multi sockets */
  // Look in /sys/devices/system/node/online for a string with format 0-1
  num_cores = 1;  // by default
  f = fopen("/sys/devices/system/node/online", "r");
  if (f) {
    int a, b;
    if (fscanf(f, "%d-%d", &a, &b) == 2) {
      num_cores = b - a + 1;
    }
    fclose(f);
  }
  
  int cores[num_cores];
  // Find one core id per numa node.
  // Look in /sys/devices/system/node/node*/cpulist 
  for(int i=0; i < num_cores; i++) {
    char b[50];
    cores[i] = -1;
    snprintf(b, sizeof(b), "/sys/devices/system/node/node%d/cpulist", i);
    f = fopen(b, "r");
    if (f) {
      fscanf(f, "%d", &cores[i]);
      fclose(f);
    }
  }

#if 0
  printf("Found %d core%s: ", num_cores, num_cores > 1 ? "s" : "");
  for(int i=0; i < num_cores; i++)
    printf("%d ", cores[i]);
  printf("\n");
#endif
  
  /* Initialize type the first time */
  if (type == -1) {
    f = fopen(PREFIX_PATH "type", "r");
    if (!f) {
      fprintf(stderr, "No perf_event support for RAPL.\n");
      return -1;
    }
    fscanf(f, "%d", &type);
    attr.type = type;
    fclose(f);
  }

  em->num_probes = 0;
  em->probes = NULL;
  em->typ = EMON_PERF;
  em->raw_reading = Probe_RAPL_raw_reading;

  for(int j = 0; j < num_cores; j++) {
    //    cores[0] = 0;

    // Iterate over domains  
    for(int i=0; i < NUM_RAPL_DOMAINS; i++) {  
      // config
      sprintf(filename, PREFIX_PATH "events/%s", domain_names[i]);
      f = fopen(filename, "r");
      if (f) {
        int config = 0;
        fscanf(f, "event=%x", &config);
        attr.config = config;
        fclose(f);
      }
      else
        continue;

      // scale
      sprintf(filename, PREFIX_PATH "events/%s.scale", domain_names[i]);
      f = fopen(filename, "r");
      if (f) {
        fscanf(f, "%lf", &scale);
        fclose(f);
      }
      else
        continue;

      // units
      sprintf(filename, PREFIX_PATH "events/%s.unit", domain_names[i]);
      f = fopen(filename,"r");
      if (f) {
        fscanf(f, "%30s", units);
        fclose(f);
      }

      /* get file descriptor for probe */
      int fd = perf_event_open(&attr, -1, cores[j], -1, 0);
      if (fd < 0) {
        if (errno == EACCES) {
          fprintf(stderr, "Permission denied: cannot use PERF_EVENT interface."
                  "\nTo use it, fix your paranoid level or run as root.\n");
        }
        break;
      }

      int n = em->num_probes++;
      em->probes = realloc(em->probes, (n+1) * sizeof(EMProbe));
      em->probes[n].name = display_names[i];
      em->probes[n].unit = strdup(units);
      em->probes[n].scale = scale;
      em->probes[n].u.fd = fd;
    }
  }

  if (em->num_probes == 0) {
    assert(em->probes == NULL);
    return -1;
  }
  return 0;
}
