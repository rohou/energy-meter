/*
 * This file is part of energy-meter.
 *
 * Author: Erven ROHOU
 * Copyright (c) 2018, 2021 Inria
 *
 * License: GNU Affero General Public License version 3.
 *
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "emon.h"
#include "probe_perfevent.h"
#include "probe_powercap.h"

EMonitor* EMon_init(enum probe_types typ)
{
  EMonitor* em = malloc(sizeof(EMonitor));
  switch(typ) {
  case EMON_PERF:
    if (init_RAPL(em) == 0)
      return em;
    break;
    
  case EMON_POWERCAP:
    if (init_powercap(em) == 0)
      return em;
    break;
    
  case EMON_ANY:
    if (init_RAPL(em) == 0)
      return em;
    else if (init_powercap(em) == 0)
      return em;
    break;
    
  default:
    fprintf(stderr, "Unknown probe type '%d'\n", typ);
  }
  free(em);
  return NULL;
}


void EMon_fini(EMonitor* em)
{
}


double EMon_reading(EMonitor* em, int i)
{
  assert(i < em->num_probes);
  EMProbe* p = &em->probes[i];
  double ret;

  double val = em->raw_reading(p);
  ret = val * p->scale;
  return ret;
}
