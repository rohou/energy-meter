/*
 * This file is part of energy-meter.
 *
 * Author: Erven ROHOU
 * Copyright (c) 2018, 2021 Inria
 *
 * License: GNU Affero General Public License version 3.
 *
 */

#ifndef _PROBE_RAPL_H
#define _PROBE_RAPL_H

int init_RAPL();


#endif  // _PROBE_RAPL_H
