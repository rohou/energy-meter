/*
 * This file is part of energy-meter.
 *
 * Author: Erven ROHOU
 * Copyright (c) 2018, 2021 Inria
 *
 * License: GNU Affero General Public License version 3.
 *
 */

#ifndef _PROBE_POWERCAP_H
#define _PROBE_POWERCAP_H


#include "emon.h"

int init_powercap();


#endif  // _PROBE_POWERCAP_H
