/*
 * This file is part of energy-meter.
 *
 * Author: Erven ROHOU
 * Copyright (c) 2018, 2021 Inria
 *
 * License: GNU Affero General Public License version 3.
 *
 */

#ifndef _EMON_H
#define _EMON_H

// This library provides access to the energy monitors available in
// x86 processors.
//
// Different interfaces exist: perf_event, powercap, direct access to
// the machine specific registers (msr).
//

enum probe_types {
  EMON_PERF,      // for Linux perf_event interface
  EMON_POWERCAP,  // for Linux powercap interface
  EMON_ANY        // for the first succeeding interface
};


typedef struct EMProbe {
  double scale;      // scale to apply to reading
  char*  name;       // name of probe
  char*  unit;       // energy unit (typically "J" for joules)
  union {
    int   fd;        // file descriptor used by perf_event
    char* filename;  // name of file used by powercap
  } u;
} EMProbe;



//
// Energy Monitor
//
typedef struct EMonitor {
  enum probe_types typ;
  int      num_probes;
  double   (*raw_reading)(struct EMProbe*);
  EMProbe* probes;
} EMonitor;


// initializers/destructor
EMonitor* EMon_init(enum probe_types);
void EMon_fini(EMonitor* pm);


double EMon_reading(EMonitor* pm, int i);


#endif  // _EMON_H
