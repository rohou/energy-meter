/*
 * This file is part of energy-meter.
 *
 * Author: Erven ROHOU
 * Copyright (c) 2018, 2021 Inria
 *
 * License: GNU Affero General Public License version 3.
 *
 */

#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#include "emon.h"
#include "probe_powercap.h"


//
// Probe_powercap
//

static double Probe_powercap_raw_reading(EMProbe* p)
{
  long long val;
  FILE*     f;

  f = fopen(p->u.filename, "r");
  if (!f) {
    perror("fopen");
    fprintf(stderr, "Cannot open '%s'\n", p->u.filename);
    return 0.0;
  }
  int n = fscanf(f, "%lld", &val);
  if (n != 1) {
    perror("fscanf");
    return 0.0;
  }
  fclose(f);
  
  return (double)val;
}


int init_powercap(EMonitor* em)
{
  //
  // Try to access energy sensors, through the powercap feature.
  // /sys/devices/virtual/powercap. Look for files 'name' and
  // 'energy_uj'.
  //
  const char* prefix = "/sys/devices/virtual/powercap/intel-rapl";

  DIR*            dir;
  struct dirent*  dirent;

  dir = opendir(prefix);
  if (!dir)
    return -1;

  em->probes = NULL;
  em->num_probes = 0;
  em->typ = EMON_POWERCAP;
  em->raw_reading = Probe_powercap_raw_reading;
  
  while ((dirent = readdir(dir))) {
    if (dirent->d_type != DT_DIR)  // not a directory
      continue;
    
    if (strncmp(dirent->d_name, "intel-rapl:", 11) == 0) {
      char  p_name[100];
      char  b[strlen(prefix) + strlen(dirent->d_name) + 20];
      snprintf(b, sizeof(b), "%s/%s/name", prefix, dirent->d_name);
      FILE* f = fopen(b, "r");
      if (f) {
        fscanf(f, "%99s", p_name);
        fclose(f);
      }
      else
        strcat(p_name, "noname??");

      int n = em->num_probes++;
      em->probes = realloc(em->probes, sizeof(EMProbe) * (n+1));

      em->probes[n].name = strdup(p_name);
      em->probes[n].unit = "J";
      em->probes[n].scale = 1e-6;  // energy in µJ in file energy_uj

      snprintf(b, sizeof(b), "%s/%s/energy_uj", prefix, dirent->d_name);
      em->probes[n].u.filename = strdup(b);

      char subdir_name[strlen(prefix) + strlen(dirent->d_name) + 10];
      snprintf(subdir_name, sizeof(subdir_name), "%s/%s",
               prefix, dirent->d_name);
      DIR* dir2 = opendir(subdir_name);
      if (!dir2)
        continue;
      
      struct dirent* dirent2;
      while ((dirent2 = readdir(dir2))) {
        if (dirent2->d_type != DT_DIR)  /* not a directory */
          continue;
        
        if (strncmp(dirent2->d_name, "intel-rapl:", 11) == 0) {
          char p_name2[100];
          char buffer2[strlen(subdir_name) + strlen(dirent2->d_name) + 20];
          snprintf(buffer2, sizeof(buffer2), "%s/%s/name",
                   subdir_name, dirent2->d_name);
          FILE* f2 = fopen(buffer2, "r");
          if (f2) {
            fscanf(f2, "%99s", p_name2);

            fclose(f2);
          }
          else
            strcat(p_name2, "noname??");

          // Probe_powercap* p = new Probe_powercap(p_name2, (char*)"J", 1e-6);
          n = em->num_probes++;
          em->probes = realloc(em->probes, sizeof(EMProbe) * (n+1));
          em->probes[n].name = strdup(p_name2);
          em->probes[n].unit = "J";
          em->probes[n].scale = 1e-6;
          
          snprintf(buffer2, sizeof(buffer2), "%s/%s/energy_uj",
                   subdir_name, dirent2->d_name);
          em->probes[n].u.filename = strdup(buffer2);
        }
      }
    }
  }
  closedir(dir);

  if (em->num_probes == 0) {
    assert(em->probes == NULL);
    return -1;
  }
  return 0;
}


