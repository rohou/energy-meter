/*
 * This file is part of energy-meter.
 *
 * Author: Erven ROHOU
 * Copyright (c) 2018, 2021 Inria
 *
 * License: GNU Affero General Public License version 3.
 *
 */

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <inttypes.h>
#include <math.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "options.h"

#include "emon.h"

struct option options;


void display_header(FILE* out, EMonitor* pm, int power, int show_time)
{
  int i;
  
  fprintf(out, "      ");
  if (show_time) {
    fprintf(out, " %10s",  "Time");
  }
  for(i=0; i < pm->num_probes; i++) {
    fprintf(out, "%10s ", pm->probes[i].name);
  }
  fprintf(out, "\n");
  fprintf(out, "      ");
  if (show_time) {
    fprintf(out, "          s");
  }
  for(i=0; i < pm->num_probes; i++) {
    if (power)
      fprintf(out, "%8s/s ", pm->probes[i].unit);
    else
      fprintf(out, "%10s ", pm->probes[i].unit);
  }
  fprintf(out, "\n");
}


int main(int argc, char* argv[])
{
  int      i, iter;
  //  int num_sockets;
  float    delay;
  FILE*    out;
  EMonitor* em;


  init_options(&options);
  parse_command_line(argc, argv, &options);

  delay = options.delay;
  out = options.out;

  //  num_sockets = 1; // FIXME

  // Initialize all probes
  em = EMon_init(EMON_ANY);

  if (!em) {
    fprintf(stderr, "Sorry, no energy probe found on the system. Aborting.\n");
    exit(EXIT_FAILURE);
  }

  if (em->typ == EMON_PERF)
    printf("Using PERF_EVENT interface.\n");
  else if (em->typ == EMON_POWERCAP)
    printf("Using POWERCAP interface.\n");
  else
    assert(0);

  int num_probes = em->num_probes;
  double prev[num_probes];
  for(i=0; i < num_probes; i++) {
    prev[i] = EMon_reading(em, i);
  }

  if (options.spawn_pos) {
    pid_t    child;
    struct timeval   t1, t2;
    double   acc[num_probes];
    double   acc_sq[num_probes];
    double   acc_time = 0.0, acc_sq_time = 0.0;


    for(i=0; i < num_probes; i++) {
      acc[i] = 0.0;
      acc_sq[i] = 0.0;
      }

    display_header(out, em, 0, options.spawn_pos);
    
    for(int r = 0; r < options.repeat; r++) {
      gettimeofday(&t1, NULL);
      child = fork();
      if (child == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
      }

      if (child == 0) {  // in the child
        argv += options.spawn_pos;
        if (execvp(argv[0], argv) == -1) {
          perror("execvp");
          exit(EXIT_FAILURE);
        }
      }
      else {  // in the parent
        wait(NULL);
        gettimeofday(&t2, NULL);
        double t = t2.tv_sec-t1.tv_sec + (t2.tv_usec - t1.tv_usec)/1000000.0;
        fprintf(out, "%5d ", r);
        fprintf(out, " %10.3f", t);
        acc_time += t;
        acc_sq_time += t*t;
        for(i=0; i < num_probes; i++) {
          double val = EMon_reading(em, i);
          double delta = val-prev[i];
          //          fprintf(out, "%10.2lf (%12f)  ", delta, val);
          fprintf(out, "%10.2lf  ", delta);

          // Debug code, sometimes negative delta, don't know why...
          if (delta < 0) {
            fprintf(stderr, "\n\n** Whoa! **\nprobe %d  val=%f  prev=%f\n", i, val, prev[i]);
            fprintf(stderr, "scale: %.12f\n", em->probes[i].scale);
            fprintf(stderr, "name: %s\n", em->probes[i].name);
            if (em->typ == EMON_POWERCAP) {
              char buffer[10 + strlen(em->probes[i].u.filename)];
              sprintf(buffer, "cat %s", em->probes[i].u.filename);
              fprintf(stderr, "file: %s\n", em->probes[i].u.filename);
              system(buffer);
            }
            fprintf(stderr, "unit: %s\n", em->probes[i].unit);
          }

          acc[i] += delta;
          acc_sq[i] += delta*delta;
          prev[i] = val;
        }
        fprintf(out, "\n");
      }
    }
    if (options.repeat > 1) {
      // Standard deviation
      double n = options.repeat;
      fprintf(out, "Stddev");
      fprintf(out, " %10.3f",
              sqrt((n * acc_sq_time - acc_time*acc_time) / (n * (n-1))));
      for(i=0; i < num_probes; i++) {
        fprintf(out, "%10.2lf ", sqrt((n*acc_sq[i] - acc[i]*acc[i])/(n*(n-1))));
      }
      fprintf(out, "\n");

      // Average
      fprintf(out, "Avg   ");
      fprintf(out, " %10.3f", acc_time / options.repeat);
      for(i=0; i < num_probes; i++) {
        fprintf(out, "%10.2lf ", acc[i] / options.repeat);
      }
      fprintf(out, "\n");
    }
    return 0;
  }

  display_header(out, em, 1, options.spawn_pos);
  iter = 0;
  while (1) {
    fprintf(out, "%5d ", iter);
    for(i=0; i < num_probes; i++) {
      double val = EMon_reading(em, i);
      // fprintf(out, "%10.2lf (%12f)  ", (val - prev[i]) / delay, val);
      fprintf(out, "%10.2lf ", (val - prev[i]) / delay);

      // Debug code, sometimes negative delta, don't know why...
      if (val < prev[i]) {
        fprintf(stderr, "\n\n** Whoa! **\nprobe %d  val=%f  prev=%f\n", i, val, prev[i]);
        fprintf(stderr, "scale: %.12f\n", em->probes[i].scale);
        fprintf(stderr, "name: %s\n", em->probes[i].name);
        if (em->typ == EMON_POWERCAP) {
          char buffer[10 + strlen(em->probes[i].u.filename)];
          sprintf(buffer, "cat %s", em->probes[i].u.filename);
          fprintf(stderr, "file: %s\n", em->probes[i].u.filename);
          system(buffer);
        }
        fprintf(stderr, "unit: %s\n", em->probes[i].unit);
      }

      prev[i] = val;
    }
    fprintf(out, "\n");
    iter++;
    usleep(delay*1000000);
  }
  
  /* Cleanup */
  EMon_fini(em);
  fclose(out);
  
  return 0;
}
