/*
 * This file is part of energy-meter.
 *
 * Author: Erven ROHOU
 * Copyright (c) 2018, 2021 Inria
 *
 * License: GNU Affero General Public License version 3.
 *
 */

#ifndef _OPTIONS_H
#define _OPTIONS_H



/* global state */
struct option {
  int    spawn_pos;
  float  delay;
  FILE*  out;
  int    repeat;
};

void init_options(struct option* opt);
void parse_command_line(int argc, char* argv[], struct option* const);


#endif  /* _OPTIONS_H */
