/*
 * This file is part of energy-meter.
 *
 * Author: Erven ROHOU
 * Copyright (c) 2018, 2021 Inria
 *
 * License: GNU Affero General Public License version 3.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "options.h"


static void usage(const char* name)
{
  fprintf(stderr, "Usage: %s [option]\n", name);
  fprintf(stderr, "       %s [option] -- command\n", name);
  fprintf(stderr, "\t-d --delay d      delay in seconds between refreshes\n");
  fprintf(stderr, "\t-h --help         print this message\n");
  fprintf(stderr, "\t-o --output file  output file\n");
  fprintf(stderr, "\t-r --repeat num   repeat command num times\n");
}


void init_options(struct option* opt)
{
  /* default status for options */
  memset(opt, 0, sizeof(*opt));
  opt->delay = 1;
  opt->out = stdout;
  opt->repeat = 1;
}


void parse_command_line(int argc, char* argv[],
                        struct option* const options)
{
  int i;

  /* Note: many flags are toggles. They invert what is in the
     configuration file. */
  for(i=1; i < argc; i++) {

    if (strcmp(argv[i], "--") == 0) {  /* command to be spawned by tiptop */
      if (argc > i+1)  /* at least something after -- */
        options->spawn_pos = i+1;
      else {
        fprintf(stderr, "No command after --, aborting.\n");
        exit(EXIT_FAILURE);
      }
      break;
    }
    
    if ((strcmp(argv[i], "-d") == 0) || (strcmp(argv[i], "--delay") == 0)) {
      if (i+1 < argc) {
        options->delay = (float)atof(argv[i+1]);
        if (options->delay < 0.01)
          options->delay = 1;
        i++;
        continue;
      }
      else {
        fprintf(stderr, "Missing delay after -d.\n");
        exit(EXIT_FAILURE);
      }
    }

    if ((strcmp(argv[i], "-h") == 0) || (strcmp(argv[i], "--help") == 0)) {
      usage(argv[0]);
      exit(0);
    }
    
    if ((strcmp(argv[i], "-o") == 0) || (strcmp(argv[i], "--output") == 0)) {
      if (i+1 < argc) {
        options->out = fopen(argv[i+1], "w");
        if (!options->out) {
          perror("fopen");
          fprintf(stderr, "Could not open '%s'\n", argv[i+1]);
          exit(EXIT_FAILURE);
        }
        i++;
        continue;
      }
      else {
        fprintf(stderr, "Missing filename after -o.\n");
        exit(EXIT_FAILURE);
      }
    }

    if ((strcmp(argv[i], "-r") == 0) || (strcmp(argv[i], "--repeat") == 0)) {
      if (i+1 < argc) {
        options->repeat = atoi(argv[i+1]);
        i++;
        continue;
      }
      else {
        fprintf(stderr, "Missing num after -r.\n");
        exit(EXIT_FAILURE);
      }
    }

  }
}
